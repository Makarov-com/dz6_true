module.exports = (hbs) => {

    hbs.registerHelper('getPhoneStyle', (data) => {
        // console.log(data[1]);
        let n = 0, numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        for (let i = 0; i < data.length; i++) {
            for (let a = 0; a < numbers.length; a++) {

                if (data[i] == numbers[a]) {
                    n++;
                }
            }
        }
        // console.log(n);
        if (n > 10) {
            return new hbs.SafeString("<th class='greenNumber'>" + data + "</th>");
        } else {
            return new hbs.SafeString("<th>" + data + "</th>");
        }
    })

    hbs.registerHelper('getCompanyStyle', (data) => {
        if (data === "Microsoft") {
            return new hbs.SafeString("<th class='green'>" + data + "</th>");
        } else {
            return new hbs.SafeString("<th>" + data + "</th>");
        }
    })

    return hbs;
}

